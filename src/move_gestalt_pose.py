#!/usr/bin/env python

import sys
import copy
import rospy
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg
from math import pi
from std_msgs.msg import String
from moveit_commander.conversions import pose_to_list

moveit_commander.roscpp_initialize(sys.argv)
rospy.init_node('move_group_python_interface', anonymous=True)

robot = moveit_commander.RobotCommander(robot_description="gestalt/robot_description")
scene = moveit_commander.PlanningSceneInterface(ns="gestalt")
group_name = "arm"    
group = moveit_commander.MoveGroupCommander(robot_description="gestalt/robot_description",ns="gestalt", name = group_name)
display_trajectory_publisher = rospy.Publisher('/move_group/display_planned_path', moveit_msgs.msg.DisplayTrajectory, queue_size=20)

group.clear_pose_targets()

# pose_target = geometry_msgs.msg.Pose()
# pose_target.orientation.w = 1.0
# pose_target.position.x = 0.9
# pose_target.position.y = -0.05
# pose_target.position.z = 10.1


# home position 
# ---------------#
# pose_target = geometry_msgs.msg.Pose()
# pose_target.position.x = 0.0010892439008030053
# pose_target.position.y = -0.24431413316473538
# pose_target.position.z = 2.6180482997683807
# pose_target.orientation.x = 0.010265760971415183
# pose_target.orientation.y = 1.0820507205115018
# pose_target.orientation.z = 0
# pose_target.orientation.w = 0

#initial position
'''
[0.001151875452520379, 
-0.24435588195582536, 
2.617918197063137, 
-0.010590211711630104, 
1.0821463973123508, 
0.0011548304178514712]
'''



rospy.loginfo("Start Position")
rospy.loginfo(group.get_current_pose())



#Position-1

pose_goal = geometry_msgs.msg.Pose()

pose_goal.orientation.w = 1
pose_goal.position.x = 0.4
pose_goal.position.y = 0.2
pose_goal.position.z = 0.6
group.set_pose_target(pose_goal)

group.go(wait=True)

group.stop()
rospy.loginfo("Finished Goal:1")
rospy.loginfo(group.get_current_pose())

rospy.sleep(3)

group.clear_pose_targets()



#Position-2

pose_goal.orientation.w = 1
pose_goal.position.x = 0.4
pose_goal.position.y = -0.2
pose_goal.position.z = 0.6
group.set_pose_target(pose_goal)

group.go(wait=True)

group.stop()
rospy.loginfo("Finished Goal:2")
rospy.sleep(3)

group.clear_pose_targets()


#Position-3

pose_goal.orientation.w = 1
pose_goal.position.x = 0.4
pose_goal.position.y = -0.2
pose_goal.position.z = 0.8
group.set_pose_target(pose_goal)

group.go(wait=True)

group.stop()
rospy.loginfo("Finished Goal:3")
rospy.sleep(3)

group.clear_pose_targets()


#Position-4

pose_goal.orientation.w = 1
pose_goal.position.x = 0.4
pose_goal.position.y = 0.2
pose_goal.position.z = 0.8
group.set_pose_target(pose_goal)

group.go(wait=True)

group.stop()
rospy.loginfo("Finished Goal:4")
rospy.sleep(3)

group.clear_pose_targets()


#Position-destination return to initial state

pose_goal.position.x = 0.0806197086451
pose_goal.position.y = -0.00204623791656
pose_goal.position.z = 0.241679094946
pose_goal.orientation.x = 0.00321208516536
pose_goal.orientation.y = 0.390768086269
pose_goal.orientation.z = -0.00455538920478
pose_goal.orientation.w = 0.920472288389

group.set_pose_target(pose_goal)

plan = group.go(wait=True)
group.stop()
rospy.loginfo("Finished Goal: Final")
group.clear_pose_targets()



moveit_commander.roscpp_shutdown()