#!/usr/bin/env python

import sys
import copy
import rospy
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg
from math import pi
from std_msgs.msg import String
from moveit_commander.conversions import pose_to_list

moveit_commander.roscpp_initialize(sys.argv)
rospy.init_node('move_group_python_interface', anonymous=True)

robot = moveit_commander.RobotCommander()
scene = moveit_commander.PlanningSceneInterface()    
group_name = "arm"    
group = moveit_commander.MoveGroupCommander("arm")
display_trajectory_publisher = rospy.Publisher('/move_group/display_planned_path', moveit_msgs.msg.DisplayTrajectory, queue_size=20)


group.set_goal_position_tolerance(0.01)
# group.set_goal_joint_tolerance(2)
# group.set_goal_position_tolerance(2)
# group.set_goal_position_tolerance(2)
group.clear_pose_targets()

# pose_target = geometry_msgs.msg.Pose()
# pose_target.orientation.w = 1.0
# pose_target.position.x = 0.9
# pose_target.position.y = -0.05
# pose_target.position.z = 10.1


# home position 
# ---------------#
# pose_target = geometry_msgs.msg.Pose()
# pose_target.position.x = 0.0010892439008030053
# pose_target.position.y = -0.24431413316473538
# pose_target.position.z = 2.6180482997683807
# pose_target.orientation.x = 0.010265760971415183
# pose_target.orientation.y = 1.0820507205115018
# pose_target.orientation.z = 0
# pose_target.orientation.w = 0

#initial position
'''
[0.001151875452520379, 
-0.24435588195582536, 
2.617918197063137, 
-0.010590211711630104, 
1.0821463973123508, 
0.0011548304178514712]
'''


#Planning_to_a_Joint_Goal

joint_goal = group.get_current_joint_values()

# rospy.loginfo(joint_goal)
print(joint_goal)


#move joint_0
joint_goal[0] = pi/16
joint_goal[1] = -1.48
joint_goal[2] = 0.87
joint_goal[3] = 0
joint_goal[4] = 1.518
joint_goal[5] = 0.015


group.go(joint_goal, wait=True)
group.stop()

rospy.sleep(5)


print("1stop")

joint_goal[0] = -pi/16
group.go(joint_goal, wait=True)
group.stop()

print("2stop")

rospy.sleep(5)

group.set_goal_tolerance(0.8)

joint_goal[0] = -1.1233207657390665e-06
joint_goal[1] = -0.2618004175098685
joint_goal[2] = 2.6005385322909893
joint_goal[3] = -0.017454409600039078
joint_goal[4] = -1.0995581589149976
joint_goal[5] = 0.032605381197226
group.go(joint_goal, wait=True)
group.stop()

print("3stop")

rospy.sleep(5)

group.set_goal_position_tolerance(0.01)


joint_goal[0] = 0.001151875452520379
joint_goal[1] = -0.24435588195582536
joint_goal[2] = 2.617918197063137
joint_goal[3] = -0.010590211711630104
joint_goal[4] = 1.0821463973123508
joint_goal[5] = 0.0011548304178514712
group.go(joint_goal, wait=True)
group.stop()

print("finish")

rospy.loginfo(group.get_goal_tolerance())


group.clear_pose_targets()
'''

#move joint_1
'''
'''
joint_goal[1] = -pi/4
group.go(joint_goal, wait=True)
group.clear_pose_targets()

joint_goal[1] = -0.24435588195582536
group.go(joint_goal, wait=True)
group.clear_pose_targets()
'''
#move joint_2
'''
joint_goal[2] = pi/2
group.go(joint_goal, wait=True)
group.clear_pose_targets()

joint_goal[2] = 2.617918197063137
group.go(joint_goal, wait=True)
group.clear_pose_targets()

#move joint_3

joint_goal[3] = pi/8
group.go(joint_goal, wait=True)
group.clear_pose_targets()

joint_goal[3] = -0.010590211711630104
group.go(joint_goal, wait=True)
group.clear_pose_targets()

#move joint_4

joint_goal[4] = pi/8
group.go(joint_goal, wait=True)
group.clear_pose_targets()

joint_goal[4] = 1.0821463973123508
group.go(joint_goal, wait=True)
group.clear_pose_targets()


#move joint_5

joint_goal[5] = pi/8
group.go(joint_goal, wait=True)
group.clear_pose_targets()

joint_goal[5] = 0.0011548304178514712
group.go(joint_goal, wait=True)
group.clear_pose_targets()


'''
#Planning_to_a_Pose_Goal

#initial state
'''
position: 
    x: 0.0806197086451
    y: -0.00204623791656
    z: 0.241679094946
  orientation: 
    x: 0.00321208516536
    y: 0.390768086269
    z: -0.00455538920478
    w: 0.920472288389
'''

#Position-1
'''
pose_goal = geometry_msgs.msg.Pose()
pose_goal.orientation.w = 1.0
pose_goal.position.x = 0.4
pose_goal.position.y = 0.1
pose_goal.position.z = 0.6

group.set_pose_target(pose_goal)

plan = group.go(wait=True)
group.stop()

group.clear_pose_targets()


#Position-2 return to initial state


pose_goal.position.x = 0.0806197086451
pose_goal.position.y = -0.00204623791656
pose_goal.position.z = 0.241679094946
pose_goal.orientation.x = 0.00321208516536
pose_goal.orientation.y = 0.390768086269
pose_goal.orientation.z = -0.00455538920478
pose_goal.orientation.w = 0.920472288389

group.set_pose_target(pose_goal)

plan = group.go(wait=True)
group.stop()

group.clear_pose_targets()

'''


moveit_commander.roscpp_shutdown()