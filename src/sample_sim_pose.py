#!/usr/bin/env python

import sys
import copy
import rospy
import tf
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg
from math import pi
from std_msgs.msg import String
from moveit_commander.conversions import pose_to_list

class arm_grasp:

    def __init__(self):

        self.robot = moveit_commander.RobotCommander()
        self.scene = moveit_commander.PlanningSceneInterface()    
        self.group_name = "arm"    
        self.group = moveit_commander.MoveGroupCommander("arm")
        self.display_trajectory_publisher = rospy.Publisher('/move_group/display_planned_path', moveit_msgs.msg.DisplayTrajectory, queue_size=20)

        self.listener = tf.TransformListener()
        self.base_frame_id = "base_link"
        self.blob_frame_id = "hand_camera"

        self.pose = None
        self.rot = None

    def get_tf_data(self):

        while (self.pose == None or self.rot == None):
            try:
                (self.pose,self.rot) = self.listener.lookupTransform(self.base_frame_id, self.blob_frame_id, rospy.Time(0))

            except (tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException):
                pass

        # print("Pose_x: " + str(pose[0]))
        # print("Pose_y: " + str(pose[1]))
        # print("Pose_z: " + str(pose[2]))

        # print("Rot_x: " + str(rot[0]))
        # print("Rot_y: " + str(rot[1]))
        # print("Rot_z: " + str(rot[2]))
        # print("Rot_w: " + str(rot[3]))


        return self.pose, self.rot


    def move_arm(self,pose,rot):

        pose_goal = geometry_msgs.msg.Pose()

        pose_goal.position.x = pose[0]
        pose_goal.position.y = pose[1]
        pose_goal.position.z = pose[2]
        pose_goal.orientation.w = rot[3]

        self.group.set_pose_target(pose_goal)
        self.group.go(wait=True)
        self.group.stop()

        rospy.loginfo("Finished Goal:1")
        # rospy.loginfo(group.get_current_pose())

        rospy.sleep(3)

        self.group.clear_pose_targets()

        moveit_commander.roscpp_shutdown()




if __name__ == '__main__':
    
    moveit_commander.roscpp_initialize(sys.argv)
    rospy.init_node('move_group_python_interface', anonymous=True)

    robot_arm = arm_grasp()

    ret_pose, ret_rot = robot_arm.get_tf_data()

    print(ret_pose)
    print(ret_rot)

    ret_pose[0] = ret_pose[0] + 0.2

    robot_arm.move_arm(ret_pose,ret_rot)

    print("finish")

    moveit_commander.roscpp_shutdown()
